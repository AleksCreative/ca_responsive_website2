<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CA_Responsive_website
 */

?>

	
    </div><!-- #content -->
	<footer id="colophon" class="site-footer" >
            
          
            <div class="redcliffe-footer">
                <div class="footer-sidebar site-footer footer-wrapper">
		<div class="footer-sidebar footer-stack3 logos-redcliffe">
                    <a href="http://cotswoldarchaeology.co.uk/" target="_blank"><img src="http://cotswoldarchaeology.co.uk/wp-content/uploads/2017/09/CA-Logo-and-text-2011-Copy.jpg" alt="Cotswold Archaeology logo"></a>
                    <a href="https://www.oxfordarchaeology.com/" target="_blank"><img src="http://cotswoldarchaeology.co.uk/wp-content/uploads/2018/07/oxford-archaeology-logo.jpg" alt="Oxford Archaeology logo"></a>
                    <a href="http://www.changerealestate.co.uk/" target="_blank"> <img src="http://cotswoldarchaeology.co.uk/wp-content/uploads/2018/07/change-real-estate-logo.jpg" alt="Change real estate logo"></a>
                    <a href="http://www.redcliffquarter.com/" target="_blank"><img src="http://cotswoldarchaeology.co.uk/wp-content/uploads/2018/08/Redcliff-Quarter-logo.jpg" alt="Redcliff Quarter logo"></a>
                        
		</div><!-- .site-info -->
                <div class="footer-site-map redcliffe-site-info footer-stack3">
                        <p>Cotswold Archaeology Ltd. Registered in England</p>
                        <p>Reg no: 2362531</p>
			<p>Registered Charity No: 1001653</p>
                        <p> &copy; <?php echo date('Y'); ?> Cotswold Archaeology. All Rights Reserved </p>
                </div>
                </div>
            </div>
        </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
