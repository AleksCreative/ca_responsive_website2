<?php
/**
 * The template for displaying archive pages
 *
 d
 *
 * @package CA_Responsive_website
 */
  get_header('community');

?>

	<div id="primary" class="content-area">
		<main class="site-main" role="main">
                    <div class="mobile-container">
                    <div class="two-third main-news">

                        <div class="main-left aligncenter padding-top20">
                            <?php echo get_the_post_thumbnail(); ?>
                        </div>
                        <div class="main-right">
                                <h2 class="publication"><?php echo get_the_title(); ?></h2>
                                <div class="book-title"><h3><?php the_field('book_title'); ?></h3></div>
                                <p><strong>Autor/Editor:</strong> <?php the_field('autoreditor'); ?></p>
                                <p><?php the_field('year'); ?></p>
                                <p><strong>Pages:</strong> <?php the_field('pages'); ?></p>
                                <p><strong>ISBN:</strong> <?php the_field('isbn'); ?></p>
                                <div class="publication-downloads"><p><strong>Links/Downloads:</strong> <?php the_field('downloads'); ?></p></div>
                                <br>
                        </div>
                        </div>
                    <div class="one-third publication-list">
                        <div class="category-list community">
                            <h6>CA Publications</h6>
                          <?php

                        $taxonomy = 'publication_category';
                        $terms = get_terms($taxonomy); // Get all terms of a taxonomy

                        if ( $terms && !is_wp_error( $terms ) ) :
                        ?>
                            <ul>
                                <?php foreach ( $terms as $term ) { ?>
                                    <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php endif;?>

                        </div>

                    </div>
                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->
		</main><!-- #main -->
	</div><!-- #primary -->
<?php

get_footer('community');
