<?php
/**
 * The template for displaying archive pages
 *
 
 *
 * @package CA_Responsive_website
 */
  get_header('contact');

?>
<div id="primary" class="content-area">
		<main id="main" class="site-main contact" role="main">
                    <div class="mobile-container">    
                    <div class="one-third">
                            
                            <div>
  
                               <?php if(get_field('office_title')): ?> 
                                <div><h1><?php the_field('office_title'); ?></h1></div>
                                    
                               <?php endif; ?>
                            </div>
                            
                            <div>
  
                               <?php if(get_field('office_address')): ?>
                                <div><?php the_field('office_address'); ?></div>
                                
                               <?php endif; ?>
                            </div>
                            
                        </div><!---.main-left panel--->
                    
                       <div class="two-third"> 
                           <div class="office_map ">
  
                               <?php if(get_field('office_map')): ?>
                                <div ><?php the_field('office_map'); ?></div>
                                
                               <?php endif; ?>
                            </div>
                        
                       </div><!---.main-right panel--->
                        <div class="clearfix"></div>
                        <div class="fullpage">
  
                               <?php if(get_field('office_info')): ?> 
                                <div><?php the_field('office_info'); ?></div>
                                    
                               <?php endif; ?>
                            </div>
                    </div>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php
get_footer('contact');
