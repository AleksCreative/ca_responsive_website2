<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('company'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main company" role="main">
                    <div id="company-main" class="fullpage main-left">
                        <div class="services-intro"><?php the_field('services_intro'); ?></div>
                    </div><!--company-main-->
                    <div class="fullpage main-right"><div id="latest-news" class=" latest-news business-news grey-box" >                                                 
                                       
                                            <h2>Company news</h2>
                                            <?php
                                                // Arguments for first post
                                                $args = array(
                                                    'posts_per_page' => 1,
                                                    'cat' => 5
                                                );
                                                // The Query for first post
                                                $query1 = new WP_Query( $args );

                                                // The Loop
                                                while ( $query1->have_posts() ) :
                                                    $query1->the_post(); ?>
                                            <?php the_title('<h4 class="home-link"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>');?>
                            <div><?php the_post_thumbnail ();?></div>
                            <div><?php the_excerpt() ; ?></div>
                            <?php endwhile; ?>
                                            <?php

                                                /* Restore original Post Data 
                                                 * NB: Because we are using new WP_Query we aren't stomping on the 
                                                 * original $wp_query and it does not need to be reset with 
                                                 * wp_reset_query(). We just need to set the post data back up with
                                                 * wp_reset_postdata().
                                                 */
                                                wp_reset_postdata(); ?>
                            <?php
                                                 
                                                // Arguments for post list
                                                $args2 = array(
                                                    'posts_per_page' => 2,
                                                    'offset'=> 1,
                                                    'cat' => 5
                                                );
                                                /* The 2nd Query (without global var) */
                                                $query2 = new WP_Query( $args2 );

                                                // The 2nd Loop
                                                while ( $query2->have_posts() ) :
                                                    $query2->the_post(); ?>
<div class="newsfeed-list">
                                        <hr>
                                        <ul>
                                            <li class="home-link newsfeed">
                                                <div class="float-left"><?php the_post_thumbnail ();?></div>
                                                <div><?php the_title('<h4 class="newsfeed"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>'); ?></div>
                                                <div><?php custom_excerpt(200); ?></div>
                                            </li>
                                        </ul>
                                    </div>
                                   
                               <div class="clearfix"></div>
                                
                                <?php endwhile; ?>
                                               
                               <?php

                                                // Restore original Post Data
                                                wp_reset_postdata();

                                                ?>
                                            
                                            

                                        </div><!-- latest news and content -->
                    </div><!--main-right panel-->
                    <div class="clearfix"></div>
                    <div class=" panel-of-four company-info">
                        <div class="one-fourth company-p1-color">
                            <div class="panel-header"><h4 ><?php the_field('spanel_1_title'); ?></h4> </div>                              
                                                             
                                <div class="panel-image "><?php if( get_field('spanel_1_image') ): ?> 
                                <img src="<?php the_field('spanel_1_image'); ?>" />
                                <?php endif; ?></div>
                         </div>       
                        <div class="one-fourth company-p2-color">
                            <div class="panel-header"><h4 ><?php the_field('spanel_2_title'); ?></h4> </div> 
                                
                                <div class="panel-image "><?php if( get_field('spanel_2_image') ): ?> 
                                <img src="<?php the_field('spanel_2_image'); ?>" />
                                <?php endif; ?></div>
                         </div>
                         <div class="one-fourth company-p3-color">
                             <div class="panel-header"><h4 ><?php the_field('spanel_3_title'); ?></h4></div>  
                                
                                <div class="panel-image"><?php if( get_field('spanel_3_image') ): ?> 
                                <img src="<?php the_field('spanel_3_image'); ?>" />
                                <?php endif; ?></div>
                                     </div>
                        <div class="one-fourth company-p4-color">
                            <div class="panel-header"><h4 ><?php the_field('spanel_4_title'); ?></h4></div> 
                                
                                <div class="panel-image "> <?php if( get_field('spanel_4_image') ): ?> 
                                <img src="<?php the_field('spanel_4_image'); ?>" />
                                <?php endif; ?></div>
                                </div>
                       
                    <div class="clearfix"></div>
                    </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('company');
