<?php

/**
 * The template for displaying archive pages for Case Studies
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */

get_header('company'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main company" role="main">
                    <div class="two-third-left fullpage">
                        <div class="services-background">
                                      <div><?php

                                            if(get_field('client1'))
                                            {
                                                    echo '<p><strong>Client: </strong>' . get_field('client1') . '</p>';
                                            }

                                            ?>  </div>
                        <div><?php

                                            if(get_field('project1'))
                                            {
                                                    echo '<p><strong>Project: </strong>' . get_field('project1') . '</p>';
                                            }

                                            ?>  </div>
                            <div><?php

                                            if(get_field('location1'))
                                            {
                                                    echo '<p><strong>Location: </strong>' . get_field('location1') . '</p>';
                                            }

                                            ?>  </div>
                            <div><?php

                                            if(get_field('services_provided1'))
                                            {
                                                    echo '<p><strong>Services provided: </strong>' . get_field('services_provided1') . '</p>';
                                            }

                                            ?>  </div>
                            </div>
                        <div>
                         <div><?php

                                            if(get_field('scope'))
                                            {
                                                    echo '<h2>Scope</h2><p>' . get_field('scope') . '</p>';
                                            }

                                            ?>  </div>
                         <div><?php

                                            if(get_field('points_of_interest'))
                                            {
                                                    echo '<h2>Points of interest</h2><p>' . get_field('points_of_interest') . '</p>';
                                            }

                                            ?>  </div>
                            <div>
                                        <?php 

                                        $images = get_field('case-study-gallery');
                                        $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)

                                        if( $images ): ?>
                                            <div class="newsfeed-list-gallery"><ul >
                                                <?php foreach( $images as $image ): ?>
                                                    <li>
                                                        <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                                                    </li>
                                                <?php endforeach; ?>
                                                </ul></div>
                                        <?php endif; ?>
                            </div>
                        
                        </div>  
                                    </div>
                                   
                                    <div class="one-third-right fullpage">
                                        <div><h4>Related Services</h4>
                                            <div><?php

                                            if(get_field('related_services'))
                                            {
                                                    echo '<p>' . get_field('related_services') . '</p>';
                                            }

                                            ?>  </div>
                                        </div>
                                        <div class="case-studies">
                                               <h4>Other Case Studies</h4>         
                                                      <?php
                                                          // Arguments for post list
                                $args3 = array(
                                    'posts_per_page' => 30,
                                    'post_type'   => 'casestudy',
                                    
                                );
                                /* The 3nd Query (without global var) */
                                $query3 = new WP_Query( $args3 );

                                // The 3nd Loop
                                while ( $query3->have_posts() ) {
                                    $query3->the_post();
                                    
                                    echo '<div class="case-studies"><p>               
                                    <a href="' . get_the_permalink() . '">' . get_the_title() .
                                            '</a></p></div>';
                                         
                                    
                                }

                                // Restore original Post Data
                                wp_reset_postdata(); ?>
                                                      
                                                    </div>
                                                    
                                    </div>
                                    <div class="clearfix"></div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('company');
