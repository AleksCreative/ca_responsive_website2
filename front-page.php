<?php
/**
 * This is a home page of Cotswold Archaeology website
 * @package CA_Responsive_website
 */

get_header('frontpage'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
                        <div class="home-page-main main-left fullpage">
                            
                            <div>
  
                               <?php if(get_field('important_info_careers')): ?> 
                                <div id="careers" class="important_info"> <?php the_field('important_info_careers'); ?></div>
                                    
                               <?php endif; ?>
                            </div>
                            
                            <div>
  
                               <?php if(get_field('important_info_outreach')): ?>
                                <div id="outreach" class="important_info"> <?php the_field('important_info_outreach'); ?></div>
                                
                               <?php endif; ?>
                            </div>
                            
                             <div>
  
                                 <?php if(get_field('important_info_services')): ?>
                                 <div id="services" class="important_info services"> <?php the_field('important_info_services'); ?></div>
                                 <?php endif; ?>
                            </div>
                            
                            <div id="welcome_to_ca_header">
  
                               <?php if(get_field('welcome_to_ca_header')){ //if the field is not empty
                                    echo '<p>' . get_field('welcome_to_ca_header') . '</p>'; //display it
                                } ?>
                            </div>
                            <div id="welcome_to_ca">
  
                               <?php if(get_field('welcome_to_ca')){ //if the field is not empty
                                    echo '<p>' . get_field('welcome_to_ca') . '</p>'; //display it
                                } ?>
                            </div>
                            
                            <div id="welcome_to_ca_image">
                                   <?php if( get_field('welcome_to_ca_image') ): ?>

                                    <img src="<?php the_field('welcome_to_ca_image'); ?>" />

                                    <?php endif; ?>
                               
                            </div>
                            <div id="our_services">
                               <!-- Text and Images -->
                               <?php if(get_field('our_services')){ //if the field is not empty
                                    echo '<p>' . get_field('our_services') . '</p>'; //display it
                                } ?>
                            </div>
                            <div><!--loop-->
                                <?php
		if ( have_posts() ) :

			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;	
                        endif; ?></div>

                        </div><!--.main-left panel-->
                    
                       <div class="home-page-news main-right fullpage">         
                        <div id="home-new-content" >
                                <?php dynamic_sidebar( 'new-content' ); ?>
                        </div><!-- new content -->
                        <div id="latest-news" class="home-news grey-box latest-news" >
                            <h2>Latest news</h2>
                            <?php
                                // Arguments for first post
                                $args = array(
                                    'posts_per_page' => 1
                                    
                                );
                                // The Query for first post
                                $query1 = new WP_Query( $args );

                                // The Loop
                                while ( $query1->have_posts() ) :
                                    $query1->the_post(); ?>
                            <?php the_title('<h4 class="home-link"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>');?>
                            <div><?php the_post_thumbnail ();?></div>
                            <div><?php the_excerpt() ; ?></div>
                            <?php endwhile; ?>
                            <?php
                                /* Restore original Post Data 
                                 * NB: Because we are using new WP_Query we aren't stomping on the 
                                 * original $wp_query and it does not need to be reset with 
                                 * wp_reset_query(). We just need to set the post data back up with
                                 * wp_reset_postdata().
                                 */
                                wp_reset_postdata(); ?>
                               <?php
                                                 
                                                // Arguments for post list
                                                $args2 = array(
                                                    'posts_per_page' => 3,
                                                    'offset'=> 1
                                                    
                                                );
                                                /* The 2nd Query (without global var) */
                                                $query2 = new WP_Query( $args2 );

                                                // The 2nd Loop
                                                while ( $query2->have_posts() ) :
                                                    $query2->the_post(); ?>
<div class="newsfeed-list">
                                        <hr>
                                        <ul>
                                            <li class="home-link newsfeed">
                                                <div class="float-left"><?php the_post_thumbnail ();?></div>
                                                <div><?php the_title('<h4 class="newsfeed"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>'); ?></div>
                                                <div><?php custom_excerpt(130); ?></div>
                                            </li>
                                        </ul>
                                    </div>
                                   
                               <div class="clearfix"></div>
                                
                                <?php endwhile; ?>
                               <?php     
                                // Restore original Post Data
                                wp_reset_postdata(); ?>

                               
                            
                                <?php dynamic_sidebar( 'home-latest' ); ?>
                        </div><!-- latest news and content -->
                       
                       </div><!--.main-right panel-->
                        <div class="clearfix"></div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer('frontpage');
