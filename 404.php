<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package CA_Responsive_website
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Page not found.', 'ca_responsive_website' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location.', 'ca_responsive_website' ); ?></p>

                                        
                                         <p>You can go back to the <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Home page</a> or try one of the links below.</p>
					<?php
                        if(is_active_sidebar('sidebar-404')){
                        dynamic_sidebar('sidebar-404');
                        }
                        ?>

					

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
