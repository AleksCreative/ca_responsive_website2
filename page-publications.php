<?php
/**
 * The template for displaying publications page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package CA_Responsive_website
 */


  get_header( 'community' );
  ?>

	<div id="primary" class="content-area">
		<main class="site-main community" role="main">
                  <div class="mobile-container">
                      <div class="two-third">
		<?php
		if ( have_posts() ) : ?>

			
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
                        </div>
                         <div class="one-third publication-list">
                        <div class="category-list community">
                            <h6>CA Publications</h6> 
                          <?php

                        $taxonomy = 'publication_category';
                        $terms = get_terms($taxonomy, 'orderby=name&hide_empty=0'); // Get all terms of a taxonomy

                        if ( $terms && !is_wp_error( $terms ) ) :
                        ?>
                            <ul>
                                <?php foreach ( $terms as $term ) { ?>
                                    <li><a href="<?php echo get_term_link($term->slug, $taxonomy); ?>"><?php echo $term->name; ?></a></li>
                                <?php } ?>
                            </ul>
                        <?php endif;?>

                        </div>
                        
                    </div>
                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('community');
