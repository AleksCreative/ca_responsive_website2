<?php
/**
 * Template Name: Archive events
 *
 *
 */

get_header('community'); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main community" role="main">
                     <div class="mobile-container">
                                    <div class="two-third-left fullpage">
                       <div class="padding-left20"><div class="page-navi">
                                                        
                                                      <div>
                        <?php dynamic_sidebar( 'sidebar-2' ); ?>
                        </div>
                                                      
                                                    </div>
                                                    
                       </div>
                           
                                           </div>
                                   
                                    <div class="one-third-right fullpage">
                                                    
                                    </div>
                                    <div class="clearfix"></div>
                    </div><!-- mobile-container -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer('community');
